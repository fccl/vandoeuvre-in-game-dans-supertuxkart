Objets pour impression 3D

- Personnage Ninachu
- Personnage Vmax
- Personnage Mei
- Médaille Ninachu
- Vidéo Ninachu

Licence : CC-BY-SA 4.0\
Autrice : Lunaire alias Lauriane ABLI-BOUYO <laurianea00@gmail.com>

Inspiré des illustrations Vandœuvre in Game 2021 à 2023 par Le Zizien alias Olivier BOURGOIS <obgraphizm@gmail.com> pour la ville de Vandœuvre-lès-Nancy.

- Ninachu : https://www.vandoeuvre.fr/wp-content/uploads/2021/08/Vandoeuvre_In_Game_2023.jpg
- Vmax : https://www.vandoeuvre.fr/wp-content/uploads/2021/08/vandoeuvre-in-game-2-1.jpg
- Mei : https://www.vandoeuvre.fr/wp-content/uploads/2023/05/vandoeuvre-in-game_2021.jpg
