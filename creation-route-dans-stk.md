**Créer circuit**

# Conception squelette du parcours

## Création de la courbe

> Cet objet se nommera **squelette**.

1. Mode `Modeling`
1. `Object Mode`.
1. `Add`, `Curve`,`Bezier`. 

![Alt text](img/creation_courbe.png "a title")

> Si l'objet n'est pas visible.

- Toujours mode `Modeling`, `Object Mode`.
- Zoomez vers le `Cursor`.
- Augmentez la taille de la courbe de Bézier avec la fonction `Scale`.

> Se placer au départ du squelette.

4. Déplacer la courbe à proximité du point de départ avec la fonction `Move`.
1. Chiffre `7` pour vu du dessus (ou bouton `Z`).
- Garder cet axe pour la suite de la création de la courbe.

> Cela permet de créer dans un premier temps le squelette en 2D.

## Dessiner la courbe

> Dans le cas où vous ne visualisez pas les points et vecteurs de la courbe, n’hésitez pas à élever ceux-ci via l'axe Z car ils sont probablement « cachés » par les objets déjà existants. Ces élévations seront retravaillées ultérieurement. **Le plus pratique est d'élever drastiquement le premier point**.

- Mode `Modeling`, `Edit Mode`.

**Liste des fonctions**

- Déplacer un point : Sélectionner touche `g` pour déplacer.
- Nouveau point : touche `e`.
- Régler le courbe de Bézier : `s`.
- Impacter le vecteur : `r`.

## Fermer la courbe

> Important à faire, sans quoi la route générée plus tard continuera à l’infini après le point d'arrivée.

- Sélectionner la route.
- `Mode Layout`, `Edit Mode`.
- Sélectionner point début et fin.
- `F` pour relier.

> Bien vérifier que la reliure n'a pas généré un point supplémentaire, sans quoi il faudra le supprimer.

## Élévations

> Pour le moment, la courbe traçant le parcours n'a pas d’élévation, il est préférable d'aligner la hauteur des courbes au-dessus des objets.

- Mode `Modeling`, `Edit Mode`.
- Sélectionner un point (clic gauche).
- Vu horizontale (X ou Y).
- Élever ou abaisser avec le fonction `Move`.

![Alt text](img/elevation_point_squelette.png "a title")

# Conception de la route à partir du squelette

> Cet objet se nommera **route**.

1. Mode `Layout`, `Add`, `Mesh`, `Plane`.
1. En bas à gauche, dans `Add plane`, mettre `1 m` dans `size`.
1. À droite, onglet `Object properties` : `Scale X = 4` ; `Y = 12`.

![Alt text](img/creer_plane_options-object.png "a title")

1. Onglet `Modifier`.
1. `Add Modifier` `Generate, Array`.
1. `Fit type` = `Fit Curve` ; `Curve` = L'objet **squelette**.
1. Cocher le bouton `Merge`.
1. `Add Modifier`, `Deform, Curve`.
1. `Curve object` = L'objet **squelette**.

![Alt text](img/creer_plane_options-modifier.png "a title")

## Superposer les origines

> Ne pas hésiter à zoomer au maximum pour aligner à l'unité près les deux objets. Le moindre écart peut avoir un impact significatif sur la route finale.

- Cliquer sur l'objet **route**.
- Superposer l'origine de la route et l'origine de l'objet **squelette** avec la fonction `move`.
- Faire de même concernant la rotation avec la fonction `Rotate`.

## Largeur de la route

- Réduire La largeur du chemin : sélectionner un point puis `Alt` + `s`?

> Si besoin de plus de précisions, ajouter des points intermédiaires.

- Sélectionner deux points puis clic droit, `Subdivision`.

## Supprimer la courbe

> Il est conseillé à ce moment de réaliser une copie de votre fichier Blender, pour éventuellement revenir dessus dans le cas que le problème de « **Multi routes** » apparaisse.

> Pour la suite, on « supprime » la courbe de Bézier **squelette**. Veillez à avoir bien finalisé au mieux le cheminement avant d'effectuer cette suppression, en effet la modification de la route sera plus complexe.

> Commencez par la jouer sécurité, en copiant la **route**. Cela permet de garder les objets d'origines sous le coude.

- Sélectionner la route copie, mode `Modeling`, `Object Mode` onglet `Modifier properties`, à droite de `Array`, petite flèche qui descend et `Apply`. 

![Alt text](img/valider_array_copie_route.png "a title")

- Faites de même avec l'option `Curve`.
- Renommer la copie en **Driveline**.

# Driveline

> La _Driveline_ correspond au cheminement que devront utiliser les coureurs. Elle représente le circuit, que notamment les personnages gérés par l'ordinateur suivront, et c'est sur cette _Driveline_ que seront ajoutés les points de départ, d'arrivée, et les points de contrôles.

> Plus d'infos sur cette page (en anglais) : https://supertuxkart.net/Talk:Theoretical_Driveline_Model

## Manipulation de la Driveline

> Cachez les autres objets pour avoir un visu confortable de la _Driveline_.

- Sélectionner `Driveline`, mode `Modeling`, `Edit mode`, touche `a`, `x`, `Only faces`.

> Repassez en `Objet Mode` pour visualiser la _Driveline_ : elle doit recouvrir le cheminement du parcours dessiné précédemment, et doit globalement être représentée par une succession de quadrilatères accolés les uns aux autres à la suite.

![Alt text](img/visu_driveling.png "a title")

**Multi routes**

> Si génération de plusieurs couches de routes comme sur la capture d'écran ci-dessus (ce qui n'est pas bon), utilisez une autre méthode avec votre copie de fichier Blender.

- Mode `Modeling`.
- `Modifiers`, `Arroy`, `Fit type` = **Fit lenght**.
- Recouvrir la longueur de la route avec `Length=*` (arrêtez-vous au **m** inférieur).
- Appliquez de nouveau `Array` et `Curve`.
- Fusionnez les points départ - arrivée.
- `Edit mode`, touche `a`, `x`, `Only faces`.

![Alt text](img/visu_driveling_alternatif.png "a title")

> Bien vérifier qu'il n'y a que les quadrilatères (4 points) ne se superposent pas, notamment dans les virages. S'il le faut retravaillez la _Driveline_ pour arriver à une succession de quadrilatères. Attention pas de triangles !

- `Modeling`, `Edit Mode`, sélectionner l'objet `Driveline`.
- `Select Mode Vertex`.
- Mettez vous en `Z`.
- Déplacer les points de sorte que les quadrilatères se succèdent bien.

# Étapes de la course

## Départ et arrivée

- `Modeling`, `Edit Mode`
- Supprimer les arêtes sur les côtés du quadrilatère au niveau du départ de la course.
- Position vue `Z` : sélection point coté départ et créer des arêtes extérieures avec touche `e`.

> Visualisez l'image officielle du projet STK ci-dessous pour comprendre ce qui est attendu.

![Alt text](https://supertuxkart.net/assets/wiki/Driveline_start.jpg "a title")

## Points de controles

> Les points de contrôles ont plusieurs utilités. Dans le cas de conception de circuit en tant qu'amateur, c'est utile pour limiter les effets d'aubaines en cas de découverte de « raccourcis » dans le décor. Positionnez sur des endroits stratégiques obligeant les coureurs à réaliser obligatoirement de grandes distances.

- `Mode layout`, `Objet Mode`.
- `Add`, `Mesh`, `Plane`.
- Déplacer le point à l'emplacement prévu pour le point de contrôle.

> Copier le point de contrôle.

- `Mode layout`.
- Sélectionner le Checkline, `Shift` + `d` (ou clic droit et `Duplicate Objects`).

![Alt text](img/depart_point-controle.png "a title")

**Vectoriser les points de contrôles**

> Il faut transformer les surfaces relatives aux points de contrôles en simples vecteurs traversant la route.

- `Mode Modeling`, `Edit Mode`.
- Sélectionnez le **Checkline**.
- `Select Mode Vertex`.
- Supprimer 2 points non adjacents.
- Deplacer les deux points restant avec fonction `Move`.

![Alt text](img/checkline_vecteur.png  "a title")

# Export dans STK

## C'est un circuit

> Indiquer à Blender qu'il s'agit d'un circuit pour STK

1. Objet `Scene Properties`.
1. Cocher `Is a SuperTuxKart Track`.

![Alt text](img/indiquer_circuit_stk.png "a title")

> Les champs apparaissant pourront être complétés ultérieurement.

## Il y a une Driveline

1. Sélectionnez l'Objet **Driveline**.
1. Onglet `Objet Properties`.
1. Onglet `SuperTuxKart Object Properties`.
1. `Type` = **Driveline (main)**.

![Alt text](img/indiquer_driveline.png "a title")

## Il y a des points de controles

> Il faut indiquer à l'extension STK les objets **Checkline** comme étant des points de contrôles. Un par un à la suite, en indiquant la suite logique dans l’ordre de passage.

1. `Sélectionnez le bon **Checkline**
1. `Objet Properties`
1. `SuperTuxKart Object Properties`
1. `Type` = **Checkline**
1. `Name` = **Checkline1, Checkline2, etc**
1. `Activate` = **Checkline+1** ou **lap** pour le dernier.

![Alt text](img/indiquer_checklines.png "a title")

> Si le Checkline suivant à insérer dans `Activate` n'existe pas, créez-le puis retourner sur le Checkline déjà édité pour le compléter.

# Problèmes rencontrés

## Textures

> Supprimer les textures d'Osm-Blender

- `Shift` + `f9`, `Blender file` sur l’icône fichiers
- Vider `Matériels` et « triangle attention »

> Dans l'idéal, importer en 3D simple dès le départ.

## Couleur piste

> Vérifier si l'orientation de l'objet n'est pas inversée

- Show overlays, cocher `Face orientation`.

> Si bleu ok, si rouge c'est à l'envers !

Tout sélectionner de l'élément, `Alt` + `n`, `Flip`.

## Ajouter des objets

Voir la [documentation officielle](https://supertuxkart.net/Making_Tracks:_Items).

Il est possible d'ajouter différents objets le long du tracé : boîtes cadeaux, nitro, bananes.
Ils peuvent être un moyen utile d'indiquer aux joueurs la direction à prendre pour continuer la course.

Pour placer un objet, poser un objet `Empty` à l'endroit où l'on souhaite voir l'objet apparaître (le surélever légèrement de la route).
Puis, dans le propriétés de l'objet (SuperTuxKart Object Properties), définir son type à l'un des types "d'items" disponibles : Nitro (small ou big), Item (Gift Box) ou Banana.
En cochant `Drop to ground`, SuperTuxKart calculera automatiquement la hauteur optimale de l'objet pour qu'il puisse être récupéré par des joueurs.
