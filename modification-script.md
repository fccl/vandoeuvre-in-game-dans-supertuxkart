# Modifications du script

Il est possible de modifier le script des textures pour ajouter de nouveaux types de faces de batiments, de nouveaux matériaux ou changer la répartition des fenêtres.

## Ajouter un nouveau type de face

1. Aller dans la fonction applyTextures(), il va être nécessaire d'y faire 2 modifications
2. Une ligne "# Différenciation des murs d'appartement et de bureaux (Modification des types de face ci-dessous)" doit être présente à 2 reprises dans cette fonction
3. En dessous de cette ligne, il est possible d'ajouter `elif "<nom_type>" in mat.name:` avec `<nom_type>`, le nom du type de face qui doit être dans le nom de la texture crée par Blosm
4. Copier les 4 lignes suivantes et les coller sous la condition nouvellement créée
5. Dans la 2e ligne, modifier le chemin d'accès menant à la texture

Exemple :
![image 1](img/md16.png)

6. Copier les 5 nouvelles lignes et les coller en dessous du bloc `if` du 2e commentaire de l'étape 2

Avec cette méthode il est aussi possible de différencier des batiments manuellement. Pour réaliser ceci il faut :

1. Créer une nouvelle texture avec un nom pas déjà utilisé (exemple : "2etages")
   ![image 2](img/md18.png)
2. Ajouter cette texture à la place de celle d'origine sur les batiments voulus (ceux de l'import 3D réaliste de Blosm)

   1. Sélectionner le batiment
   2. Passer en mode `Edition`
   3. Sélectionner une face du batiment
   4. Cliquer sur `Parcourir matériau à lier`
   5. Chosir le nouveau matériau

   ![image 3](img/md17.png)
3. Modifier le script selon les étapes 1 à 6 précedemment expliquées
4. Exécuter le script

## Ajouter de nouveaux matériaux

1. Aller dans la fonction applyTextures(), il va être nécessaire d'y faire 2 modifications
2. Une ligne "# Différenciation des matériaux (Modification des matériaux ci-dessous)" doit être présente à 2 reprises dans cette fonction
3. En dessous de cette ligne, il est possible d'ajouter `elif "<nom_materiau>" in mat.name:` avec `<nom_materiau>`, le nom du matériau qui doit être dans le nom de la texture crée par Blosm
4. Copier la ligne du définissant `path` et modifier le chemin d'accès pour lier la texture voulue

Exemple:
![image 4](img/md19.png)

## Répartition des fenêtres

Fonctionnement de base du script :
Le script détermine le nombre de textures qu'il devra empiler en hauteur selon la taille du batiment dans Blender.
Pour toutes les faces à 4 côtés, la texture est appliquée 3 fois en largeur, et le nombre de fois calculé en hauteur par face

### Modifications

1. Aller dans la fonction UVMaps()
2. Dans la ligne `etages = round(dimensions[2]/6.25)`, le 6.25 représente la hauteur d'une texture en mètres, il est possible de modifier cette valeur. De plus `round` permet d'arrondir pour n'avoir que des nombres entiers, enlever cette fonction peut permettre d'avoir des nomnbres de textures non entières.
   Par la suite le script va prendre chaque point d'une face et le déplacer pour afficher le bon nombre de face, il est aussi possible de le modifier
3. Les lignes a modifier sont celles semblables à `l[uv_layer].uv = (l[uv_layer].uv[0] + 2, l[uv_layer].uv[1]*etages)`, ici le `+ 2` indique que la texture va être appliquée 3 fois en largeur (1 initial + 2). Modifier cette valeur ou la rendre dynamique peut permettre de changer le nombre de fenêtre en largeur par face. Il est aussi possible de modifier le `*etages` pour changer le nombre de textures en hauteur. Il est notamment possible, comme pour la largeur de le modifier en `+2`, faisant que tous les batiments auraient 3 étages par exemple.
