# Création de texture

* Afin d'être reconnues par le jeu, toutes les textures doivent avoir des dimensions en puissance de 2 (ex : 512x512 ou même 64x1024) et au format .png ou .jpg

## Importer la texture dans Blender

1. Aller dans le `Propriétés du matériau` d'un objet
2. Ajouter un nouveau matériau grâce au `+`
3. Cliquer sur `+ Nouveau`
4. Mode `Shading`

![image 1](img/md4.jpg)

5. Ajouter le bloc `Texture image` (`Ajouter`>`Texture`>`Texture image`)
6. Lier le point jaune `Couleur` du bloc `Texture image` au point jaune `Couleur de base` du bloc `BSDF guidée`

![image 2](img/md5.jpg)

7. Il est aussi possible de donner un nom au matériau (ici à la place de Material.002)

## Utilisation du matériau

* Si l'objet (batiment, route etc...) nécessite une seule texture, il suffit de la séléctionner et que ce soit la seule texture présente dans le menu `Propriétés du matériau`
* Sinon, il faut ajouter toutes les textures nécessaires puis :
1. Passer en mode `édition`
2. Mode `Sélection de faces`
3. Sélectionner la face désirée, puis dans le menu `Propriétés du matériau`, sélectionner la texture désirée et cliquer sur `Assigner`