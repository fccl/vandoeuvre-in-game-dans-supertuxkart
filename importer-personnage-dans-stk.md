**Importer un personnage dans STK**

> La méthodologie consiste à importer un modèle déjà existant pour notamment de l’impression 3D, puis le retravailler pour l’importer dans le jeu.

> Note : À ce jour, il n'y a pas de méthodologie pour animer le kart. Cela n'a pas été inclus dans l'expérience d'origine car est très compliqué.

# Blender

## Retoucher le personnage

### Régénérer une face

- Sélection des points puis `F`.

## Couleurs

### Définies par des matériaux

- Dans l'onglet `Matérial Properties`

![Alt text](img/couleurs_material_properties.png "a title")

- Afficher objet avec `Viewport Shading` pour visualiser les couleurs.
- Ajouter matériel : `New`
- `Base color` - `Image Texture`

![Alt text](img/couleurs_images-texture.png "a title")

- `New`
- `Name` : nom de la couleur `Widht et Height` : **32 px x 32 px**. Choisir `Color`.

![Alt text](img/couleur_new-image.png "a title")

### Définir couleur sur une surface

- `Face select`
- Sélectionner les faces
- Sélection bonne surface et `Assign`

![Alt text](img/couleur_assign.png "a title")

#### Infos pratiques

**Sélection des faces**

- Plusieurs faces individuellement : Maj + Clic gauche
- Faces entre les deux sélections : Ctrl + Clic gauche
- Zone de faces : Maj + Sélection zone Clic gauche

**Si oubli ou ajout de face sur un matériel**

- Sélectionner le matériel, puis `Assign`

### Enregistrer images couleurs

- `Textures Paint`, `Image`, `Save As...`

![Alt text](img/couleur_save-as.png "a title")

- Retour sur `Modeling`, pour éditer couleurs.

### Couleur métal et lumineux

> Dans l'extension STK

- Se situe en dessous du menu de couleur, Menu Antartica /SuperTuxkart Properties

> Nécessite d’attribuer une couleur à la surface pour activer l’option.

- `Shader` : `PBR Solid`
- PBR Data Map : Mettre le nom du fichier (exemple : lumineux.png)

![Alt text](img/effet-couleur_pbr_data-map.png "a title")

> Voir doc STK page Materials https://supertuxkart.net/Materials#gloss-map

Exemple une surface lumineuse à 75 % : Fonction `Glowing` à 75 % (~175/255) : 0.0.175

### squelette de l'objet

- Aller dans `Modeling`, `Edit mode`

#### Redimensionner l'objet

- Touche A
- Avec clic gauche `Scale` puis `Move` pour positionner Z à zéro.
- Centrer si besoin sur la ligne rouge

#### Ajouter armature

- `Add`, `Armature`
- Layout et mode objet
- Cliquer sur l'objet
- Ctrl clic gauche Armature
- Ctrl P

> L'armature deviendra le parent de l'objet

- Choisir `With`, `Automat Weights`
- Sélectionner `Aramtur`, Remplacer `Objet Mod` par `Pose Mode`
- Sélectionner l'objet central dans l'objet
Touche **I**, `Location, Rotation et Scale`.

## Intégration dans le jeu

## Export du kart

> Un fichier compatible avec STK sera généré.

- Onglet `Scene Properties`
- Cocher `Is a STK Kart`

**Options**

- `Name` : Le nom du kart
- `Group` : Add-Ons

> Add-Ons est le nom par défaut pour être placé avec l'ensemble des extensions dans le menu de STK. Vous pouvez indiquer Standard ou tout autre nouveau groupe selon vos souhaits.

- `Icon` et `Minimap Icon` : **image PNG 128 px x 128 px**

> Concernant les icônes, il est possible d'indiquer le même fichier image. Le chemin est relatif au dossier d'export.

- `Shadow` : Laisser par défaut

- `Color` : Laisser par défaut

- `Lean max` : Inclinaison maximale en degrés (exemple 30)

> L'inclinaison permet au kart de s'incliner dans les virages.

- `Exhaust particle file` : Fumée par défaut du moteur, si rien effacer le fichier.

## Tester le kart

> Peut varier selon les systèmes, l'exemple se passe sur Debian.

- Se rendre dans `~/.local/share/supertuxkart/addons/karts/`
- Créer un nouveau dossier, y copier les fichiers d'exports précédents.
- Y ajouter l'ensemble des images couleurs que vous aviez exportées.
- Démarré SuperTuxKart.

> Si le personnage est trop gros ou trop petit, il faudra le redimensionner.
