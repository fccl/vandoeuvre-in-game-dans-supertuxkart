# Importer un objet dans Blender

> Ce guide s'appui sur un import d'éléments depuis le logiciel LeoCAD

## Supprimer les mini cylindres des pièces

> Cela pour alléger la complexité du fichier

- Sélectionner les points de ce qu'on veu garder (exemple la route et le coin de la plaque)
- `Ctrl` + `l` pour sélectionner les objets liés
- Cacher ceux-ci avec `h`.
- Supprimer avec `a`, `x`, `Vertices`.
- Réaficher la plaque avec `Alt` + `h`.

## Cloner un objet

> Permet d'en importer qu'un dans STK tout en l'exploitant à plusieurs endroits simultanément. Aussi, les modification sur l'un impact les autres.

- Sélectionner l'objet, `Alt` + `d`.
