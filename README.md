# Vandœuvre in Game dans SuperTuxKart

### Prérequis

- Avoir installé [Blender](https://www.blender.org/)
- Avoir installé [SuperTuxKart Blender Addons](https://github.com/supertuxkart/stk-blender)
- Avoir installé [SuperTuxKart](https://supertuxkart.net)

## Import personnages dans SuperTuxKart

![Illustration des karts de Vandœuvre in Game](karts/karts_vig.jpg "Illustration des karts de Vandœuvre in Game")

> La méthodologie consiste à importer un modèle déjà existant pour notamment de l’impression 3D, puis le retravailler pour l’importer dans le jeu.

Documentation : [Importer un personnage dans STK](importer-personnage-dans-stk.md)

## Création et import circuit dans SuperTuxKart

### Prérequis

- Avoir installé [Blender-OSM](https://github.com/vvoovv/blosm)

### Traitement de l'import Blosm

> Dans Blender, rendre les éléments importés à l'aide de Blosm plus cohérent avec la réalité, notamment en manipulant les dénivelés.

Documentation : [Traitement de l'importation Blosm](traitement-import-blosm.md)

### Création de la route dans SuperTuxKart

> Dans Blender, ajouter le cheminement de la course puis configurer l'ensemble pour le rendre compatible avec SuperTuxKart.

Documentation : [Creation de la route dans STK](creation-route-dans-stk.md)

### Ajouter des textures sur les éléments

> Pour être lisible par le jeu, il faut ajouter des textures respectant certaines conditions. Celles-ci sont spécifiées dans cette documentation, ainsi que la méthode pour les appliquer dans Blender

Documentation : [Création de textures](creation-de-texture.md)

### Utilisation de l'extension applicant les textures

> Ce script permet d'automatiquement appliquer les textures aux baitments après import Blosm.

Documentation : [Utilisation du script](utilisation-extension.md)

### Modifier le script des textures

> Si les textures ne s'affichent pas comme voulu (exemple : trop de fenêtres ou d'étages), il est possible de modifier des endroits clés du script.

Documentation : [Modification script](modification-script.md)

### Retouches manuelles du circuit

> Quelques retouches peuvent être nécessaire après l'utilisation du script.

Documentation : [Retouches du circuit](retouches-circuit.md)

### Importer des objets dans SuperTuxKart

> Le parcours est fonctionnel, les éléments (bâtiments, routes) sont distingués par des couleurs et images. Désormais, place à l'importation d'éléments supplémentaires pour agrémenter le circuit et le paysage.

Documentation : [Import d'objets dans STK](import_objets_dans_stk.md)

### Importer des modèles de la librairie du jeu

> Pour décorer le circuit, il est possible d'ajouter des modèles de la librairie du jeu. Celle-ci comporte beacoup de modèles 3D et textures utilisables pour les circuits

Documentation : [Importer des modèles de librairie](importer-modele.md)

### Scène du circuit

> Le parcours est complété. L'idée est désormais de le mettre en valeur en générant une scène de l'ensemble. Idéal pour réaliser une promotion vidéo de la création, ou par exemple réaliser un montage permettant de visualiser les évolutions du circuit au fil du temps.

Documentation : [Création d'animation](creation-animation.md)
