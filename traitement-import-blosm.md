**Traitement import Blosm**

> Traitement des données importées via Blender-OSM (Bâtiments, routes, autres).

# Modifier polygones bâtiments

> Exemple intervenir sur les structures qui se superposent mais dont le niveau du sol ne correspond pas (les bâtiments sont à recaler en hauteur par rapport au sol).

## Séparer les polygones

1. Cacher les objets qui ne sont pas des bâtiments.
1. Passer en `Edit mode`.
1. Touche `a` pour tout sélectionner.
1. Touche `p`, puis `By Loose Parts`.

![Séparer les polygones des bâtiments](img/separer-polygones-batiments.png)

> Une autre solution consiste à sélectionner l’objet bâtiment en `Objet Mode`, puis en passant en `Edit mode`, continuer la même procédure.

Désormais en repassant en `Objet Mode`, vous devriez pouvoir sélectionner un polygone / bâtiment unique. Ils sont par ailleurs listés dans les objets.

## Aligner les polygones en hauteur pour avoir une cohérence avec l’ensemble.

> Si cela est possible, trouver des vues en 360°, orthophotos obliques, photos sur le terrain… pour mieux représenter la réalité.

1. Passer sur l’onglet `Layout` (cela affiche le menu « transform »).
1. Sélectionner un objet polygone / bâtiment.
1. Activer la fonctionnalité `Move`.
1. Déplacer en hauteur l’objet avec la flèche directionnelle bleu (Axe hauteur – Z).
1. Le menu `Transform` indique la localisation `Location` pour chaque axe de l’objet sélectionné par rapport à son emplacement d’origine.

> Il peut être pratique de passer dans une vue permettant l’alignement graphique de la hauteur des objets (axe X ou Y).

- Sélectionner pour cela l’un des deux axes `X` ou `Y`.
- Il est possible de se déplacer dans le visuel sans impacter la vue, en faisant Shift + Clic gauche + déplacement via la souris.

> Aligner plusieurs objets en hauteur par rapport à leurs bases. Sélectionner l’objet qui servira de base pour l’alignement relatif en dernier.

1. Sélectionner les objets (Clic gauche + Maj maintenu).
1. `Object`, `Transform`, `Align Objects` (une fenêtre s’ouvre).
1. Sélectionner `Align Mode` = `Negative Sides`, `Align` = `Z`.

## Supprimer faces

> Utile par exemple dans le cas de présence de préaus, actuellement Blosm ne sait pas supprimer les murs qui sont inexistants dans la réalité (https://github.com/vvoovv/blosm/issues/372).

1. Être en `Edit Mode`.
1. Sélectionner `Select Mode - Face Select`.
1. Sélectionner les faces avec clic gauche.
1. Clic droit puis `Delete Faces`.

![Supprimer les faces des bâtiments](img/supprimer-faces-batiments.png)

## Découper face

> Utile par exemple pour conserver qu’une partie de la face, créer un passage propre pour la piste dans les bâtiments.

1. Être en `Edit Mode`.
1. Sélectionner `Select Mode - Face Select`.
1. Sélectionner la face avec clic gauche.
1. Activer la fonction `Knife` (ou touche `k`).
1. Cliquer sur les bords de la face que l’on souhaite découper en deux.
1. Touche `Entrée` pour valider la découpe.
1. Retourner sur la fonction `Select Box`.
1. Sélection la moitié de face à supprimer.
1. Clic gauche puis `Delete Faces`.

![Découper les faces des bâtiments](img/decouper_faces_batiments.png)

# Modifier les zones, chemins et routes

> Dans Blosm, est importable les zones piétonnes, zones de verdures, chemins pétons, pistes cyclables, différentes niveaux de route (service, secondaire, principale, etc). Des surfaces sont visibles, cependant elles ne sont pas indépendantes, elles sont liées à des points, qui quant à eux ne sont pas visibles par défaut. Ce sont ces points, reliés par des vecteurs, qui déterminent les surfaces (hauteurs, largeurs, etc).

`Fixme : Est-ce que Blosm prend en charge les valeurs de largeurs indiquées sur les routes dans OpenStreetMap ?`

## Afficher ces points et vecteurs

1. Passez sur l’onglet `Modeling`; `Objet Mode`
2. Filtrez les objets pour n’afficher que le type de chemin souhaité
3. Sélectionnez l’objet souhaité
2. Passez en `Edit Mode`

Exemple avec des chemins

![Afficher les points et vecteurs des chemins](img/afficher-points-vecteurs-chemins.png)

Exemple avec des surfaces

![Afficher les surfaces des surfaces](img/afficher-surface-surfaces.png)

## Il est possible de

- Supprimer des points.
- Supprimer des vecteurs.
- Supprimer des surfaces.
- Relier des points ou surfaces (sélectionnez points ou arrêtes puis `f`).
- Déplacer avec la fonction Move (**sauf la hauteur qui est liée au terrain**).
- Dupliquer un point, un vecteur, une surface avec `Maj + D`, puis clic gauche à l’endroit souhaité (si le chemin ou la surface ne s’affiche pas, élevez la copie avec l’axe Z).
- Ajouter un point à un vecteur, en selectionnant deux points, puis `Subdivide` via le clic droit.
- Modifier la largeur des chemins avec la fonction `Radius` - `Alt +s` (Possible de sélectionner plusieurs points).

# Élévation du terrain

> Sur des zones ayants d’une part des dénivelés, et d’autre part des assemblages de bâtiments complexes, il faudra certainement effectuer des corrections sur l’élévation du terrain. Le terrain est constitué de divers tuiles carrés assemblées. C’est sur ce terrain que sont calées en hauteur les routes, zones piétonnes , etc, et l’imagerie ou le fond de carte.
> Pour ne pas déformer les objets calés, éviter de déplacer les tuiles autre qu’en hauteur.

1. Passez sur l'onglet `Modeling` ou `Layout`.
2. `Objet Mode`.
3. Sélectionnez l’objet `Terrain`.
4. Passez en `Edit Mode`.
5. `Select Mode - Vertex` (point).
6. Sélectionnez un point
7. Fonction `Move`, élevé en axe Z.

![Élévation du terrain](img/elevation-terrain.png)

> Si les tuiles sont trop grandes, il existe plusieurs façons pour les découper, la plus pratique semble être la fonction de couteau. Vous pourrez après découpage déplacer les points plus finement.

1. En `Edit Mode`, `Select Mode - Face`.
2. Sélectionnez la face à découper avec clic gauche.
3. Fonction `Knife` - `k`.
4. Dessinez la forme souhaitée en débutant et terminant sur une arrête.
5. Validez avec la touche Entrée.

# Convertir les chemins en surfaces

> Contrairement aux surfaces, il n'est pas possible d'attribuer des textures aux chemins, pour le moment géré par des vecteurs annexes.
> La solution consiste à les convertir en surface.

1. Selectionner l'objet.
2. Mode `Modeling`.
3. `Objetct Mode`.
4. `Object`.
5. `Convert`, `Mesh`.

![Convertir les chemins en surfaces](img/convertir_chemin_en_surface.png)

**Les couleurs de ces objets :**

> Concernant les couleurs en général, se référer à la page **[Importer un personnage dans SuperTuxKart](importer-personnage-dans-stk.md)**, Chapitre **Couleurs**.

> Vérifiez que vous appliquer la texture à la surface et non au vecteur que vous avez supprimé

1. `Material Properties`.
2. `Link`, sélectionnez `Object`.

![Couleur, option Link - Object](img/couleur_link_objet.png)
