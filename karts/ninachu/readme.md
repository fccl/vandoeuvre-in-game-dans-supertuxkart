# Ninachu

> Ninachu, mascotte de l’événement Vandœuvre in Game 2023

![Karting de Ninachu](ninachu.jpg)

## Licence

Licence : CC-BY-SA 4.0\
Autrice : Copyleft 2023 Lunaire\
Autrice-Contact : Lunaire alias Lauriane ABLI-BOUYO <laurianea00@gmail.com>\
Fichiers : Files: kart.xml materials.xml 2023.spm wheel-front-left.spm wheel-rear-right.spm\
Propriété intellectuelle : Copyleft 2023 Lunaire\
Licence : CC-BY-SA 4.0

Commentaire : Inspiré de l'illustration Vandœuvre in Game 2023 (https://www.vandoeuvre.fr/wp-content/uploads/2021/08/Vandoeuvre_In_Game_2023.jpg) par Le Zizien alias Olivier BOURGOIS <obgraphizm@gmail.com> pour la ville de Vandœuvre-lès-Nancy.

## Description module

Le module original n’est pas disponible dans le dépôt d’extensions officielles du projet SuperTuxKart pour les problématiques liées aux licences de certains éléments.\
Le module original est disponible uniquement par le biais de ce dépôt.

Le module « édulcoré » est disponible sur la forge de SuperTuxKart depuis ce lien https://online.supertuxkart.net/addons.php?type=karts&name=ninachu
