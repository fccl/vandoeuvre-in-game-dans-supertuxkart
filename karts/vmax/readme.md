# Vmax

> Vmax, mascotte de l’événement Vandœuvre in Game 2022.

![Karting de Vmax](vmax.jpg)

## Licence

Licence : CC-BY-SA 4.0\
Autrice : Copyleft 2023 Lunaire\
Autrice-Contact : Lunaire alias Lauriane ABLI-BOUYO <laurianea00@gmail.com>\
Fichiers : kkart.xml materials.xml vmax.spm stk_conelight_a.png__.spm stk_conelight_a.png__.001.spm\
Propriété intellectuelle : Copyleft 2023 Lunaire\
Licence : CC-BY-SA 4.0

Commentaire : Inspiré de l'illustration Vandœuvre in Game 2022 (https://www.vandoeuvre.fr/wp-content/uploads/2021/08/vandoeuvre-in-game-2-1.jpg) par Le Zizien alias Olivier BOURGOIS <obgraphizm@gmail.com> pour la ville de Vandœuvre-lès-Nancy.

## Description module

Le module est disponible sur la forge de SuperTuxKart depuis ce lien https://online.supertuxkart.net/addons.php?type=karts&name=vmax
