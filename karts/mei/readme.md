# Mei

> Mei, mascotte de l’événement Vandœuvre in Game 2021. Dans les fichiers du module, une manipulation pour ajouter un effet spécial est indiquée.

![Karting de Mei](mei.jpg)

## Licence

Licence : CC-BY-SA 4.0\
Autrice : Copyleft 2023 Lunaire\
Autrice-Contact : Lunaire alias Lauriane ABLI-BOUYO <laurianea00@gmail.com>\
Fichiers : kart.xml materials.xml 2021.spm stk_conelight_a.png__.spm\
Propriété intellectuelle : Copyleft 2023 Lunaire\
Licence : CC-BY-SA 4.0

Commentaire : Inspiré de l'illustration Vandœuvre in Game 2021 (https://www.vandoeuvre.fr/wp-content/uploads/2023/05/vandoeuvre-in-game_2021.jpg) par Le Zizien alias Olivier BOURGOIS <obgraphizm@gmail.com> pour la ville de Vandœuvre-lès-Nancy.

## Description module

Le module est disponible sur la forge de SuperTuxKart depuis ce lien https://online.supertuxkart.net/addons.php?type=karts&name=mei
