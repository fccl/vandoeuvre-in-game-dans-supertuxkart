# Permet la création de l'add-on
bl_info = {
    "name": "Import des textures",
    "author": "Léo Gueffier",
    "version": (1,0),
    "blender": (3,4,1),
    "location": "VIEW3D>UI",
    "description": "Ajoute des textures à Blosm reconnues par le jeu SuperTuxKart",
    "warning": "",
    "wiki_url": "",
    "category": "Import-Export",
}

import bpy
import bmesh
import bl_ui
import os
import webcolors
import time
from pathlib import Path
from random import randint
from bpy.props import (StringProperty,
                       BoolProperty,
                       IntProperty,
                       FloatProperty,
                       FloatVectorProperty,
                       EnumProperty,
                       PointerProperty,
                       )

from bpy.types import (Panel,
                       Operator,
                       AddonPreferences,
                       PropertyGroup,
                       )

def init():

    collectionFound = False
    for collection in bpy.data.collections:
        if collection.name == "building":
            collectionFound = True
            break
    # Si la collection building existe déjà, déplace tous les batiments qui ne sont pas déjà dans la collection building dedans
    if collectionFound == True:
        building = bpy.data.collections["building"]
        for collection in bpy.data.collections:
            for obj in collection.all_objects:
                  if "osm_buildings" in obj.name:
                        map = bpy.data.collections[collection.name]
                        map.objects.unlink(obj)
                        building.objects.link(obj)
    # Si la collection building n'existe pas, créé cette collection et y déplace tous les batiments
    elif collectionFound == False:
        myCol = bpy.data.collections.new("building")
        bpy.context.scene.collection.children.link(myCol)
        building = bpy.data.collections["building"]
        for collection in bpy.data.collections:
            for obj in collection.all_objects:
                  if "osm_buildings" in obj.name:
                        map = bpy.data.collections[collection.name]
                        map.objects.unlink(obj)
                        building.objects.link(obj)

# Applique des textures aux batiments selon leurs caractéristiques
def applyTexture():
    building = bpy.data.collections["building"] # Collection des batiments
    textures = [] # Liste contenant les couleurs de batiments (nom des textures de l'import 3D simple)
    createdTextures = [] # Liste contenant le nom des textures déjà créées afin d'éviter de le créer de nouveau
    roofs = ["roof","gravel","concrete","metal"]
    switch = True
    num = 0
    for obj in building.all_objects:
        if obj.type == 'MESH':
            if switch:
                bpy.ops.object.mode_set(mode='OBJECT')
                # Concerne uniquement le premier batiment de chaque import
                if obj.name[-1] == "s":
                    if len(textures) == 0:
                        # Ajoute la couleur à la liste
                        slot = obj.material_slots[obj.data.polygons[1].material_index]
                        mat = slot.material
                        textures.append(mat.name)
                        # Sélectionne l'objet pour pouvoir supprimer tous les batiments doublons
                        obj.select_set(True)
                    else:
                        # Suppression de tous les batiments du premier import lorsque le script arrive au premier batiment de l'import 3D réaliste
                        bpy.ops.object.delete()
                        # Permet de séparer les batiments du premier et second import
                        switch = False
                        # Si le batiment a une couleur spécifique, récupère celle-ci et la stocke sous format RGB
                        if textures[num] != "wall" and textures[num] != "roof":
                            try:
                                rgb = webcolors.name_to_rgb(textures[num])
                            except:
                                rgb = tuple(int(textures[num][i:i+2], 16) for i in (0, 2, 4))
                            wallColor = textures[num]
                        # Si le batiment n'a pas de couleur, applique automatiquement la couleur blanche
                        else:
                            rgb = webcolors.name_to_rgb("white")
                            wallColor = "white"
                        # Création du nom de la texture et ajout aux textures créées
                        temp = obj.material_slots[obj.data.polygons[1].material_index].material.name.replace("_",".").split(".")
                        tex = temp[0] + wallColor
                        createdTextures.append(tex)
                        indexes = []
                        # Vérification des faces différentes des murs pour repérer les toits
                        for i in range (len(obj.data.polygons)):
                            if any(x in obj.material_slots[obj.data.polygons[i].material_index].material.name for x in roofs):
                                indexes.append(i)
                        tuiles = False
                        # Création de la texture de toits
                        pathTex = ["//assets/default/textures/cladding/concrete/duion_concrete_rough_01_D.png","//assets/default/textures/cladding/roof tiles/rubberduck_220.jpg"]
                        for i in range(2):
                            if i==0:
                               mat = bpy.data.materials.new(name="toit")
                            else:
                               mat = bpy.data.materials.new(name="toit2")
                            mat.use_nodes = True
                            texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                            principled_bsdf = mat.node_tree.nodes.get("Principled BSDF")
                            mat.node_tree.links.new(texture_node.outputs["Color"], principled_bsdf.inputs["Base Color"])
                            texture_node.image = bpy.data.images.load(pathTex[i])
                        if len(indexes) > 1:
                            tuiles = True
                        # Suppression des matériaux déjà existants sur le batiment
                        for i in range(len(obj.material_slots)):
                            bpy.ops.object.material_slot_remove({'object': obj})
                        # Création de la texture des murs
                        bpy.data.materials.new(name=tex)
                        mat = bpy.data.materials.get(tex)
                        mat.use_nodes = True
                        shaderColor = mat.node_tree.nodes.new(type="ShaderNodeHueSaturation")
                        mix = mat.node_tree.nodes.new(type="ShaderNodeMixRGB")
                        mix2 = mat.node_tree.nodes.new(type="ShaderNodeMixRGB")
                        texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                        # Différenciation des matériaux (Modification des matériaux ci-dessous)
                        if "brick" in mat.name:
                            path = bpy.path.abspath("//assets/default/textures/cladding/brick/cc0textures_bricks11_col.jpg")
                        else:
                            path = bpy.path.abspath("//assets/default/textures/cladding/plaster/duion_plaster_stucco_01_D.png")
                        texture_node.image = bpy.data.images.load(path)
                        texture_node.image = bpy.data.images.load(path)
                        texture_node2 = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                        shaderColor.inputs[4].default_value = (rgb[0]/255,rgb[1]/255,rgb[2]/255,1)
                        principled_bsdf = mat.node_tree.nodes.get("Principled BSDF")
                        mat.node_tree.links.new(shaderColor.outputs["Color"], mix.inputs["Color1"])
                        mat.node_tree.links.new(texture_node.outputs["Color"], mix.inputs["Color2"])
                        mat.node_tree.links.new(mix.outputs["Color"], mix2.inputs["Color1"])
                        mat.node_tree.links.new(texture_node2.outputs["Color"], mix2.inputs["Color2"])
                        mat.node_tree.links.new(mix2.outputs["Color"], principled_bsdf.inputs["Base Color"])
                        # Différenciation des murs d'appartement et de bureaux (Modification des types de face ci-dessous)
                        if "appartments" in mat.name:
                            texture_node2 = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                            texture_node2.image = bpy.data.images.load("//assets/default/textures/facades/appartments/pro_6908.png")
                            mat.node_tree.links.new(texture_node2.outputs["Color"], mix2.inputs["Color2"])
                            mat.node_tree.links.new(texture_node2.outputs["Alpha"], mix2.inputs["Fac"])
                        else:
                            texture_node2 = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                            texture_node2.image = bpy.data.images.load("//assets/default/textures/facades/office/pro_2161.png")
                            mat.node_tree.links.new(texture_node2.outputs["Color"], mix2.inputs["Color2"])
                            mat.node_tree.links.new(texture_node2.outputs["Alpha"], mix2.inputs["Fac"])
                        obj.data.materials.append(mat)
                        if tuiles:
                            obj.data.materials.append(bpy.data.materials.get("toit2"))
                        else:
                            obj.data.materials.append(bpy.data.materials.get("toit"))
                        # Apllique la texture de toit aux face concernées
                        for i in range (len(indexes)):
                            obj.data.polygons[indexes[i]].material_index = 1
                        # Suppression des attributs changeant la coloration des batiments aléatoirement
                        if obj.data.color_attributes:
                             for attr in obj.data.color_attributes:
                                  obj.data.color_attributes.remove(attr)
                        num = num+1
                # Répétion des étapes précédantes pour les batiments suivants
                else:
                    slot = obj.material_slots[obj.data.polygons[1].material_index]
                    mat = slot.material
                    textures.append(mat.name)
                    obj.select_set(True)
            else:
                if textures[num] != "wall" and textures[num] != "roof":
                    try:
                        rgb = webcolors.name_to_rgb(textures[num])
                    except:
                        rgb = tuple(int(textures[num][i:i+2], 16) for i in (0, 2, 4))
                    wallColor = textures[num]
                else:
                    rgb = webcolors.name_to_rgb("white")
                    wallColor = "white"
                temp = obj.material_slots[obj.data.polygons[1].material_index].material.name.replace("_",".").split(".")
                tex = temp[0] + wallColor
                indexes = []
                for i in range (len(obj.data.polygons)):
                    if any(x in obj.material_slots[obj.data.polygons[i].material_index].material.name for x in roofs):
                        indexes.append(i)
                for i in range(len(obj.material_slots)):
                    bpy.ops.object.material_slot_remove({'object': obj})
                isCreated = False
                for texture in createdTextures:
                    if tex == texture:
                        isCreated = True
                if isCreated:
                    mat = bpy.data.materials.get(tex)
                else:
                    createdTextures.append(tex)
                    bpy.data.materials.new(name=tex)
                    mat = bpy.data.materials.get(tex)
                    mat.use_nodes = True
                    shaderColor = mat.node_tree.nodes.new(type="ShaderNodeHueSaturation")
                    mix = mat.node_tree.nodes.new(type="ShaderNodeMixRGB")
                    mix2 = mat.node_tree.nodes.new(type="ShaderNodeMixRGB")
                    texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                    # Différenciation des matériaux (Modification des matériaux ci-dessous)
                    if "brick" in mat.name:
                        path = bpy.path.abspath("//assets/default/textures/cladding/brick/cc0textures_bricks11_col.jpg")
                    else:
                        path = bpy.path.abspath("//assets/default/textures/cladding/plaster/duion_plaster_stucco_01_D.png")
                    texture_node.image = bpy.data.images.load(path)
                    texture_node2 = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                    shaderColor.inputs[4].default_value = (rgb[0]/255,rgb[1]/255,rgb[2]/255,1)
                    principled_bsdf = mat.node_tree.nodes.get("Principled BSDF")
                    mat.node_tree.links.new(shaderColor.outputs["Color"], mix.inputs["Color1"])
                    mat.node_tree.links.new(texture_node.outputs["Color"], mix.inputs["Color2"])
                    mat.node_tree.links.new(mix.outputs["Color"], mix2.inputs["Color1"])
                    mat.node_tree.links.new(texture_node2.outputs["Color"], mix2.inputs["Color2"])
                    mat.node_tree.links.new(mix2.outputs["Color"], principled_bsdf.inputs["Base Color"])
                    # Différenciation des murs d'appartement et de bureaux (Modification des types de face ci-dessous)
                    if "appartments" in mat.name:
                        texture_node2 = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                        texture_node2.image = bpy.data.images.load("//assets/default/textures/facades/appartments/pro_6908.png")
                        mat.node_tree.links.new(texture_node2.outputs["Color"], mix2.inputs["Color2"])
                        mat.node_tree.links.new(texture_node2.outputs["Alpha"], mix2.inputs["Fac"])
                    else:
                        texture_node2 = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                        texture_node2.image = bpy.data.images.load("//assets/default/textures/facades/office/pro_2161.png")
                        mat.node_tree.links.new(texture_node2.outputs["Color"], mix2.inputs["Color2"])
                        mat.node_tree.links.new(texture_node2.outputs["Alpha"], mix2.inputs["Fac"])
                obj.data.materials.append(mat)
                if len(indexes) > 1:
                    obj.data.materials.append(bpy.data.materials.get("toit2"))
                else:
                    obj.data.materials.append(bpy.data.materials.get("toit"))
                for i in range (len(indexes)):
                    obj.data.polygons[indexes[i]].material_index = 1
                if obj.data.color_attributes:
                     for attr in obj.data.color_attributes:
                          obj.data.color_attributes.remove(attr)
                num = num+1
    # Ajout des maps UV
    UVMaps()
    # Suppression des matériaux inutilisés, évitant certains bugs
    for material in bpy.data.materials:
        if not material.users:
            bpy.data.materials.remove(material)
    # Suppression des librairies externes qui empeche l'export de la piste
    for lib in bpy.data.libraries:
        bpy.data.batch_remove(ids=(lib,))

def bakeTextures():
    # Mise en place des paramètres pour rendre le bake possible
    bpy.context.scene.render.engine = 'CYCLES'
    bpy.context.scene.cycles.use_denoising = False
    bpy.context.scene.cycles.use_preview_denoising = False
    bpy.context.scene.cycles.bake_type = 'DIFFUSE'
    bpy.context.scene.render.bake.use_pass_direct = False
    bpy.context.scene.render.bake.use_pass_indirect = False
    # Liste contenant le nom des matériaux pour éviter de le recréer plusieurs fois
    nameList = []
    building = bpy.data.collections["building"]
    for obj in building.all_objects:
        if obj.type == 'MESH':
            for slot in obj.material_slots:
                isCreated = False
                mat = slot.material
                image_name = mat.name + '_BakedTexture'
                # Teste si le matériaux est dans la liste, s'il a déjà été créé
                for name in nameList:
                    if mat.name == name:
                        isCreated = True
                if not isCreated:
                    # Création du chemin du fichier
                    path = bpy.path.abspath('//textures/bake/' + image_name + ".png")
                    my_file = Path(path)
                    nameList.append(mat.name)
                    # Vérification que le fichier n'ai pas déjà été créé auparavant
                    if not my_file.is_file():
                        bake = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
                        img = bpy.data.images.new(image_name,512,512)
                        bake.select = True
                        nodes = mat.node_tree.nodes
                        nodes.active = bake
                        bake.image = img
                        # Ajout d'un plan permettant de correctement générer l'image
                        bpy.ops.mesh.primitive_plane_add()
                        bpy.data.objects['Plane'].select_set(True)
                        ob = bpy.context.active_object
                        bpy.context.view_layer.objects.active = ob
                        ob.data.materials.append(mat)
                        # Création de l'image puis export en fichier
                        bpy.ops.object.bake(type='DIFFUSE', save_mode='EXTERNAL')
                        img.save_render(filepath=path)
                        # Suppression du plan (qui est toujours sélectionné)
                        bpy.ops.object.delete()
                    else:
                        # Si le matériau a déjà été créé, il suffit de récuperer le noeud sera utilisé pour y charger l'image en fichier
                        bake = mat.node_tree.nodes.get("Image Texture")
                    # Charge l'image en fichier pour qu'elle soit lu par SuperTuxKart
                    principled_bsdf = mat.node_tree.nodes.get("Principled BSDF")
                    bake.image = bpy.data.images.load(path)
                    mat.node_tree.links.new(bake.outputs["Color"], principled_bsdf.inputs["Base Color"])

# Créé les maps UV (utile que la première fois)
def UVMaps():
    building = bpy.data.collections["building"] # Collection des batiments
    for obj in building.all_objects:
        if obj.type == 'MESH':
            bpy.context.view_layer.objects.active = obj
            # Suppression des anciennes UV maps pour éviter les problèmes
            for i in range (3):
                obj.data.uv_layers.remove(obj.data.uv_layers[0])
            # Création d'une map vierge qui va pouvoir etre modifiée
            bpy.ops.mesh.uv_texture_add()
            bpy.ops.object.mode_set(mode='EDIT')
            # Modification de la map UV afin de prendre en compte les étages et la largeur du batiment
            uv_layer = obj.data.uv_layers.active.data
            bm = bmesh.from_edit_mesh(obj.data)
            uv_layer = bm.loops.layers.uv.verify()
            dimensions = obj.dimensions
            # Détermine le nombre d'étages du batiment
            etages = round(dimensions[2]/6.25)
            # Modidication de la map UV point par point pour chaque face
            for f in bm.faces:
                # Sélection des faces de toit
                if "toit" in obj.material_slots[f.material_index].material.name:
                    f.select_set(True)
                # Modification de la map UV permettant d'ajouter des fenetres selon le nombre d'étage
                else:
                    if len(f.loops)<5:
                        for l in f.loops:
                            if l[uv_layer].uv[0] == 0 and l[uv_layer].uv[1] == 1:
                                l[uv_layer].uv = (l[uv_layer].uv[0], l[uv_layer].uv[1]*etages)
                            elif l[uv_layer].uv[0] == 1 and l[uv_layer].uv[1] == 1:
                                l[uv_layer].uv = (l[uv_layer].uv[0] + 2, l[uv_layer].uv[1]*etages)
                            elif l[uv_layer].uv[0] == 1 and l[uv_layer].uv[1] == 0:
                                l[uv_layer].uv = (l[uv_layer].uv[0] + 2, l[uv_layer].uv[1])
                    else:
                        f.select_set(True)
            # Modification pour les faces de toits, évitant les textures étirées aléatoirement
            bpy.ops.uv.smart_project()
            for f in bm.faces:
                if "toit" in obj.material_slots[f.material_index].material.name:
                    for l in f.loops:
                        l[uv_layer].uv *= 10
                else:
                    if len(f.loops)>4:
                        for l in f.loops:
                            l[uv_layer].uv *= 3

            bmesh.update_edit_mesh(obj.data)
            bpy.ops.object.mode_set(mode='OBJECT')

# Permet d'importer les textures à partir de 2 chemins définis par l'utilisateur et des textures de bases pour les routes
def createTextures():
    path = bpy.path.abspath("//textures/routes/")
    # Import des textures de route
    for filename in os.listdir(path):
        bpy.data.materials.new(name=filename)
        mat = bpy.data.materials.get(filename)
        mat.use_nodes = True
        image = bpy.data.images.load(path)
        texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
        texture_node.image = bpy.data.images.load(path+filename)
        principled_bsdf = mat.node_tree.nodes.get("Principled BSDF")
        mat.node_tree.links.new(texture_node.outputs["Color"], principled_bsdf.inputs["Base Color"])

def createRoads():
    bpy.ops.object.select_all(action='DESELECT')
    for col in bpy.data.collections:
        if "map" in col.name:
            for obj in col.all_objects:
                if "osm_roads" in obj.name:
                    # Convertir les routes en élements modifiables en 3D
                    if obj.type == 'CURVE':
                        bpy.context.view_layer.objects.active = obj
                        selection = obj.select_get()
                        obj.select_set(True)
                        bpy.ops.object.convert(target='MESH')
                        obj.select_set(False)
                    if obj.type == 'MESH':
                        bpy.context.view_layer.objects.active = obj
                        # Suppression des anciennes UV maps pour éviter les problèmes
                        for l in obj.data.uv_layers:
                            obj.data.uv_layers.remove(l)
                        # Création d'une map vierge qui va pouvoir etre facilement modifiée
                        bpy.ops.mesh.uv_texture_add()
                        obj.select_set(True)
                        dimensions = obj.dimensions
                        # Ajoute du volume et change la forme de la route
                        bpy.ops.object.mode_set(mode='EDIT')
                        bpy.ops.mesh.select_all(action='SELECT')
                        bpy.ops.mesh.extrude_context_move(MESH_OT_extrude_context={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 3), "orient_axis_ortho":'X', "orient_type":'NORMAL', "orient_matrix":((0.543437, 0.83945, -0), (-0.83945, 0.543438, 0), (0, 0, 1)), "orient_matrix_type":'NORMAL', "constraint_axis":(False, False, True), "mirror":False, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_elements":{'INCREMENT'}, "use_snap_project":False, "snap_target":'CLOSEST', "use_snap_self":True, "use_snap_edit":True, "use_snap_nonedit":True, "use_snap_selectable":False, "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "view2d_edge_pan":False, "release_confirm":True, "use_accurate":False, "use_automerge_and_split":False})
                        bpy.ops.mesh.bevel(offset=0.89, offset_pct=0, release_confirm=True)
                        bpy.ops.mesh.select_all(action='SELECT')
                        bpy.ops.transform.translate(value=(-0, -0, -3), orient_axis_ortho='X', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, snap_elements={'INCREMENT'}, use_snap_project=False, snap_target='CLOSEST', use_snap_self=True, use_snap_edit=True, use_snap_nonedit=True, use_snap_selectable=False, release_confirm=True)
                        bpy.ops.object.mode_set(mode='OBJECT')
                        obj.select_set(False)
                        for i in range(len(obj.material_slots)):
                            bpy.ops.object.material_slot_remove({'object': obj})
                        # Appliquer les bons matériaux à chaque type de route
                        if "secondary" in obj.name:
                            mat = bpy.data.materials.get("route.png")
                            obj.data.materials.append(mat)
                            mat = bpy.data.materials.get("passage pieton.png")
                            obj.data.materials.append(mat)
                        else:
                            mat = bpy.data.materials.get("petiteRoute.png")
                            obj.data.materials.append(mat)
                            mat = bpy.data.materials.get("passage pieton2.png")
                            obj.data.materials.append(mat)
                elif "osm_paths" in obj.name:
                    if obj.type == 'CURVE':
                        bpy.context.view_layer.objects.active = obj
                        selection = obj.select_get()
                        obj.select_set(True)
                        bpy.ops.object.convert(target='MESH')
                        obj.select_set(False)
                    if obj.type == 'MESH':
                        obj.select_set(True)
                        dimensions = obj.dimensions
                        bpy.ops.object.mode_set(mode='EDIT')
                        bpy.ops.mesh.select_all(action='SELECT')
                        bpy.ops.mesh.extrude_context_move(MESH_OT_extrude_context={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 3), "orient_axis_ortho":'X', "orient_type":'NORMAL', "orient_matrix":((0.543437, 0.83945, -0), (-0.83945, 0.543438, 0), (0, 0, 1)), "orient_matrix_type":'NORMAL', "constraint_axis":(False, False, True), "mirror":False, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_elements":{'INCREMENT'}, "use_snap_project":False, "snap_target":'CLOSEST', "use_snap_self":True, "use_snap_edit":True, "use_snap_nonedit":True, "use_snap_selectable":False, "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "view2d_edge_pan":False, "release_confirm":True, "use_accurate":False, "use_automerge_and_split":False})
                        bpy.ops.mesh.bevel(offset=0.23, offset_pct=0, release_confirm=True)
                        bpy.ops.mesh.select_all(action='SELECT')
                        bpy.ops.transform.translate(value=(-0, -0, -3), orient_axis_ortho='X', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, snap_elements={'INCREMENT'}, use_snap_project=False, snap_target='CLOSEST', use_snap_self=True, use_snap_edit=True, use_snap_nonedit=True, use_snap_selectable=False, release_confirm=True)
                        bpy.ops.object.mode_set(mode='OBJECT')
                        obj.select_set(False)
                        for i in range(len(obj.material_slots)):
                            bpy.ops.object.material_slot_remove({'object': obj})
                        if "cycleway" in obj.name:
                            mat = bpy.data.materials.get("pisteCyclable.png")
                            obj.data.materials.append(mat)
                            mat = bpy.data.materials.get("pisteCyclable2.png")
                            obj.data.materials.append(mat)
                        else:
                            mat = bpy.data.materials.get("piéton.png")
                            obj.data.materials.append(mat)


# Création d'un panel pour utiliser les fonctions simplement

# Bouton final pour appliquer les textures
class bouton(bpy.types.Operator):
    bl_label = "bouton"
    bl_idname = "object.bouton"
    bl_description = "Applique les textures aux batiments"

    def execute(self, context):
        applyTexture()
        return {'FINISHED'}

class boutonBake(bpy.types.Operator):
    bl_label = "boutonBake"
    bl_idname = "object.boutonbake"
    bl_description = "Exporte les images des textures"

    def execute(self, context):
        bakeTextures()
        return {'FINISHED'}

# Bouton permettant d'ajouter les maps UV aux batiments
class boutonUV(bpy.types.Operator):
    bl_label = "boutonUV"
    bl_idname = "object.boutonuv"
    bl_description = "Nécessaire uniquement la première fois"

    def execute(self, context):
        init()
        return {'FINISHED'}

class boutonRoute(bpy.types.Operator):
    bl_label = "boutonRoute"
    bl_idname = "object.boutonroute"
    bl_description = "Donne du volume aux routes et importe les textures"

    def execute(self, context):
        global matList
        matList = createTextures()
        createRoads()
        return {'FINISHED'}

# Création du panel front-end
class panel(bpy.types.Panel):
    pass
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Import des textures"
    bl_idname = "panel"
    bl_category = "Textures"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        mytool = scene.my_tool
        row = layout.row()
        row.label(text="Textures")
        row = layout.row()
        row.operator("object.boutonuv", text="Initialisation")
        row = layout.row()
        row.operator("object.bouton", text="Ajouter textures")
        row = layout.row()
        row.operator("object.boutonroute", text="Routes")
        row = layout.row()
        row.operator("object.boutonbake", text="Exporter images")

# Enregistrement des classes

classes = (
    bouton,
    boutonBake,
    boutonUV,
    boutonRoute,
    panel,
)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)

    del bpy.types.Scene.my_tool

if __name__ == "__main__":
    register()