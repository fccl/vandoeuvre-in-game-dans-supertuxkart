# Permet la création de l'add-on
bl_info = {
    "name": "Import des textures",
    "author": "Léo Gueffier",
    "version": (1,0),
    "blender": (3,4,1),
    "location": "VIEW3D>UI",
    "description": "Ajoute des textures à Blosm reconnues par le jeu SuperTuxKart",
    "warning": "",
    "wiki_url": "",
    "category": "Import-Export",
}

import bpy
import bmesh
import bl_ui
import os 
from random import randint
from bpy.props import (StringProperty,
                       BoolProperty,
                       IntProperty,
                       FloatProperty,
                       FloatVectorProperty,
                       EnumProperty,
                       PointerProperty,
                       )

from bpy.types import (Panel,
                       Operator,
                       AddonPreferences,
                       PropertyGroup,
                       )

def init():
#     Permet de séparer les batiments en plusieurs objets (problème de crash)
#     for col in bpy.data.collections:
#        for obj in col.all_objects:
#            if "osm_buildings" in obj.name:
#                if obj.type == 'MESH':
#                    bpy.ops.mesh.separate(type="LOOSE")
    
    # Récupère la collection de la map crée par Blosm                
    for collection in bpy.data.collections:
        if "map" in collection.name:
            map = bpy.data.collections[collection.name]
    collectionFound = False
    # Si la collection building existe déjà, déplace tous les batiments qui ne sont pas déjà dans la collection building dedans
    for myCol in bpy.data.collections:
        if myCol.name == "building":
            collectionFound = True
            building = bpy.data.collections["building"]
            for obj in collection.all_objects:
                  if "osm_buildings" in obj.name:
                      if obj.users_collection != "(bpy.data.collections['building'],)":
                            map.objects.unlink(obj)
                            building.objects.link(obj)
    # Si la collection building n'existe pas, créer cette collection et y déplace tous les batiments        
    if collectionFound == False:
        myCol = bpy.data.collections.new("building")
        bpy.context.scene.collection.children.link(myCol)
        building = bpy.data.collections["building"] 
        for collection in bpy.data.collections:
            for obj in collection.all_objects:
                  if "osm_buildings" in obj.name:
                        map.objects.unlink(obj)
                        building.objects.link(obj)

# Applique des textures aléatoires aux batiments
def applyTexture(boolBuilding = True, boolPasserelle = False, BUILDING_GROUP = 5):
    building = bpy.data.collections["building"] # Collection des batiments
    altern = 0
    if boolBuilding:
        for obj in building.all_objects:      
            if obj.type == 'MESH':
                # Choix des textures aux batiments, prises aléatoirement dans la liste "facades"
                bpy.context.view_layer.objects.active = obj
                randTexture = randint(0,len(matList[0])-1)
                # Permet de faire en sorte que les batiments proches aient en général la meme texture
                if altern % BUILDING_GROUP == 0:
                    mat = bpy.data.materials.get(matList[0][randTexture])
                altern += 1
                obj.select_set(True) 
                bpy.ops.object.mode_set(mode='OBJECT')
                obj.active_material_index = 0
                # Suppression de toutes les textures déjà présentes pour éviter les bugs
                for i in range(len(obj.material_slots)):
                    bpy.ops.object.material_slot_remove({'object': obj})
                bpy.ops.object.mode_set(mode='EDIT')
                # Application de la texture au batiment
                obj.data.materials.append(mat)
                bpy.ops.object.mode_set(mode='OBJECT')

    if boolPasserelle:
        alternate = 0
        for obj in passerelle.all_objects:
            if obj.type == 'MESH':
                # Choix des textures aux batiments, alternant entre le metal rouge et beige
                bpy.context.view_layer.objects.active = obj
                if alternate % 2 == 1:
                    mat = bpy.data.materials.get("metalRouge")
                else:
                    mat = bpy.data.materials.get("metalBeige")
                alternate += 1 
                obj.select_set(True) 
                bpy.ops.object.mode_set(mode='OBJECT')
                obj.active_material_index = 0
                # Suppression de toutes les textures déjà présentes pour éviter les bugs
                for i in range(len(obj.material_slots)):
                    bpy.ops.object.material_slot_remove({'object': obj})
                bpy.ops.object.mode_set(mode='EDIT')
                # Application de la texture au batiment
                obj.data.materials.append(mat)

# Applique les textures aux toits    
def roofTexture(boolBuilding = True, passerelle = False, BUILDING_GROUP = 5):
    building = bpy.data.collections["building"] # Collection des batiments
    altern = 0
    if boolBuilding:
        for obj in building.all_objects:
            if obj.type == 'MESH':
                # Selection de la face correspondant au toit du batiment
                bpy.context.view_layer.objects.active = obj
                bpy.ops.object.mode_set(mode='EDIT')
                bpy.ops.mesh.select_all(action='DESELECT')
                obj.data.polygons[0].select = True
                bpy.ops.object.mode_set(mode='OBJECT')
                # Choix et application de la texture du toit sur cette face
                if altern % BUILDING_GROUP == 0:
                    randTexture = randint(0,len(matList[1])-1)
                altern += 1
                mat = bpy.data.materials.get(matList[1][randTexture])
                obj.data.materials.append(mat)
                obj.data.polygons[len(obj.data.polygons)-1].material_index = 1

# Créé les maps UV (utile que la première fois)            
def UVMaps():
    building = bpy.data.collections["building"] # Collection des batiments
    for obj in building.all_objects:
        if obj.type == 'MESH':
            bpy.context.view_layer.objects.active = obj
            # Suppression des anciennes UV maps pour éviter les problèmes
            for l in obj.data.uv_layers:
                obj.data.uv_layers.remove(l)
            # Création d'une map vierge qui va pouvoir etre facilement modifiée
            bpy.ops.mesh.uv_texture_add()
            bpy.ops.object.mode_set(mode='EDIT')                
            # Modification de la map UV afin de prendre en compte les étages et la largeur du batiment
            uv_layer = obj.data.uv_layers.active.data
            bm = bmesh.from_edit_mesh(obj.data)
            uv_layer = bm.loops.layers.uv.verify()
            dimensions = obj.dimensions
            # Détermine le nombre d'étages du batiment
            etages = round(dimensions[2]/6.25)
            # Modidication de la map UV point par point pour chaque face
            for f in bm.faces:
                for l in f.loops:
                    if l[uv_layer].uv[0] == 0 and l[uv_layer].uv[1] == 1:
                        l[uv_layer].uv = (l[uv_layer].uv[0], l[uv_layer].uv[1]*etages)
                    elif l[uv_layer].uv[0] == 1 and l[uv_layer].uv[1] == 1:
                        l[uv_layer].uv = (l[uv_layer].uv[0] + 2, l[uv_layer].uv[1]*etages)
                    elif l[uv_layer].uv[0] == 1 and l[uv_layer].uv[1] == 0:
                        l[uv_layer].uv = (l[uv_layer].uv[0] + 2, l[uv_layer].uv[1])
            bmesh.update_edit_mesh(obj.data)
            bpy.ops.object.mode_set(mode='OBJECT')

# Permet d'importer les textures à partir de 2 chemins définis par l'utilisateur et des textures de bases pour les routes  
def createTextures(pathFacade,pathToit):
    # Supprime toutes les textures deja existantes
    for material in bpy.data.materials:
        if not material.users:
            bpy.data.materials.remove(material)
    # Création de la liste qui va etre rendue par la fonction
    materials = [[],[]]
    # Import des textures de facade
    for filename in os.listdir(pathFacade):
        bpy.data.materials.new(name=filename)
        # Ajout des noms des matériaux à la liste des matériaux pour pouvoir les sélectionner aléatoirement par la suite
        materials[0].append(filename)
        mat = bpy.data.materials.get(filename)
        mat.use_nodes = True
        image = bpy.data.images.load(pathFacade)
        texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
        texture_node.image = bpy.data.images.load(pathFacade+filename)
        principled_bsdf = mat.node_tree.nodes.get("Principled BSDF")
        mat.node_tree.links.new(texture_node.outputs["Color"], principled_bsdf.inputs["Base Color"])
    # Import des textures de toit
    for filename in os.listdir(pathToit):
        bpy.data.materials.new(name=filename)
        materials[1].append(filename)
        mat = bpy.data.materials.get(filename)
        mat.use_nodes = True
        image = bpy.data.images.load(pathToit)
        texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
        texture_node.image = bpy.data.images.load(pathToit+filename)
        principled_bsdf = mat.node_tree.nodes.get("Principled BSDF")
        mat.node_tree.links.new(texture_node.outputs["Color"], principled_bsdf.inputs["Base Color"])
    path = bpy.path.abspath("//textures/routes/")
    # Import des textures de route
    for filename in os.listdir(path):
        bpy.data.materials.new(name=filename)
        mat = bpy.data.materials.get(filename)
        mat.use_nodes = True
        image = bpy.data.images.load(path)
        texture_node = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
        texture_node.image = bpy.data.images.load(path+filename)
        principled_bsdf = mat.node_tree.nodes.get("Principled BSDF")
        mat.node_tree.links.new(texture_node.outputs["Color"], principled_bsdf.inputs["Base Color"])
    # Renvoie la liste remplie des textures disponibles pour l'aléatoire
    return materials

def createRoads():
    for col in bpy.data.collections:
        if "map" in col.name:
            for obj in col.all_objects:
                if "osm_roads" in obj.name:
                    # Convertir les routes en élements modifiables en 3D
                    if obj.type == 'CURVE':
                        bpy.context.view_layer.objects.active = obj
                        selection = obj.select_get()
                        obj.select_set(True) 
                        bpy.ops.object.convert(target='MESH')
                        obj.select_set(False)
                    if obj.type == 'MESH':
                        bpy.context.view_layer.objects.active = obj
                        # Suppression des anciennes UV maps pour éviter les problèmes
                        for l in obj.data.uv_layers:
                            obj.data.uv_layers.remove(l)
                        # Création d'une map vierge qui va pouvoir etre facilement modifiée
                        bpy.ops.mesh.uv_texture_add()
                        obj.select_set(True) 
                        dimensions = obj.dimensions
                        # Ajoute du volume et change la forme de la routes
                        bpy.ops.object.mode_set(mode='EDIT')
                        bpy.ops.mesh.select_all(action='SELECT')
                        bpy.ops.mesh.extrude_context_move(MESH_OT_extrude_context={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 3), "orient_axis_ortho":'X', "orient_type":'NORMAL', "orient_matrix":((0.543437, 0.83945, -0), (-0.83945, 0.543438, 0), (0, 0, 1)), "orient_matrix_type":'NORMAL', "constraint_axis":(False, False, True), "mirror":False, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_elements":{'INCREMENT'}, "use_snap_project":False, "snap_target":'CLOSEST', "use_snap_self":True, "use_snap_edit":True, "use_snap_nonedit":True, "use_snap_selectable":False, "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "view2d_edge_pan":False, "release_confirm":True, "use_accurate":False, "use_automerge_and_split":False})
                        bpy.ops.mesh.bevel(offset=0.89, offset_pct=0, release_confirm=True)
                        bpy.ops.mesh.select_all(action='SELECT')
                        bpy.ops.transform.translate(value=(-0, -0, -3), orient_axis_ortho='X', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, snap_elements={'INCREMENT'}, use_snap_project=False, snap_target='CLOSEST', use_snap_self=True, use_snap_edit=True, use_snap_nonedit=True, use_snap_selectable=False, release_confirm=True)
                        bpy.ops.object.mode_set(mode='OBJECT')
                        obj.select_set(False)
                        for i in range(len(obj.material_slots)):
                            bpy.ops.object.material_slot_remove({'object': obj})
                        # Appliquer les bons matériaux à chaque type de route
                        if "secondary" in obj.name:
                            mat = bpy.data.materials.get("route.png")
                            obj.data.materials.append(mat)
                            mat = bpy.data.materials.get("passage pieton.png")
                            obj.data.materials.append(mat)
                        else:
                            mat = bpy.data.materials.get("petiteRoute.png")
                            obj.data.materials.append(mat)
                            mat = bpy.data.materials.get("passage pieton2.png")
                            obj.data.materials.append(mat)
                elif "osm_paths" in obj.name:
                    if obj.type == 'CURVE':
                        bpy.context.view_layer.objects.active = obj
                        selection = obj.select_get()
                        obj.select_set(True) 
                        bpy.ops.object.convert(target='MESH')
                        obj.select_set(False)
                    if obj.type == 'MESH':
                        obj.select_set(True) 
                        dimensions = obj.dimensions
                        bpy.ops.object.mode_set(mode='EDIT')
                        bpy.ops.mesh.select_all(action='SELECT')
                        bpy.ops.mesh.extrude_context_move(MESH_OT_extrude_context={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, TRANSFORM_OT_translate={"value":(0, 0, 3), "orient_axis_ortho":'X', "orient_type":'NORMAL', "orient_matrix":((0.543437, 0.83945, -0), (-0.83945, 0.543438, 0), (0, 0, 1)), "orient_matrix_type":'NORMAL', "constraint_axis":(False, False, True), "mirror":False, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_elements":{'INCREMENT'}, "use_snap_project":False, "snap_target":'CLOSEST', "use_snap_self":True, "use_snap_edit":True, "use_snap_nonedit":True, "use_snap_selectable":False, "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "view2d_edge_pan":False, "release_confirm":True, "use_accurate":False, "use_automerge_and_split":False})
                        bpy.ops.mesh.bevel(offset=0.89, offset_pct=0, release_confirm=True)
                        bpy.ops.mesh.select_all(action='SELECT')
                        bpy.ops.transform.translate(value=(-0, -0, -3), orient_axis_ortho='X', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, False, True), mirror=False, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False, snap=False, snap_elements={'INCREMENT'}, use_snap_project=False, snap_target='CLOSEST', use_snap_self=True, use_snap_edit=True, use_snap_nonedit=True, use_snap_selectable=False, release_confirm=True)
                        bpy.ops.object.mode_set(mode='OBJECT')
                        obj.select_set(False)
                        for i in range(len(obj.material_slots)):
                            bpy.ops.object.material_slot_remove({'object': obj})
                        if "cycleway" in obj.name:
                            mat = bpy.data.materials.get("pisteCyclable.png")
                            obj.data.materials.append(mat)
                            mat = bpy.data.materials.get("pisteCyclable2.png")
                            obj.data.materials.append(mat)
                        else: 
                            mat = bpy.data.materials.get("piéton.png")
                            obj.data.materials.append(mat)


# Création d'un panel pour utiliser les fonctions simplement

# Classe pour les inputs
class MySettings(PropertyGroup):

    group : IntProperty(
        name="Building group",
        description="Nombre de batiments par groupe",
        default=5
    )

    facade: bpy.props.StringProperty(
        name = "Dossier des textures des facades",
        subtype = 'FILE_PATH',
        description = "Chemin d'accès aux textures, format jpg ou png, résolution en puissance de 2 (exemple : 512x512)"
    )

    toit: bpy.props.StringProperty(
        name = "Dossier des textures des toits",
        subtype = 'FILE_PATH',
        description = "Chemin d'accès aux textures, format jpg ou png, résolution en puissance de 2 (exemple : 512x512)"
    )

# Bouton final pour appliquer les textures
class bouton(bpy.types.Operator):
    bl_label = "bouton"
    bl_idname = "object.bouton"
    bl_description = "Applique les textures aux batiments"

    def execute(self, context):
        UVMaps()
        applyTexture(True, False, buildingGroup)
        roofTexture(True, False, buildingGroup)
        return {'FINISHED'}

# Bouton permettant d'ajouter les maps UV aux batiments
class boutonUV(bpy.types.Operator):
    bl_label = "boutonUV"
    bl_idname = "object.boutonuv"
    bl_description = "Nécessaire uniquement la première fois"

    def execute(self, context):
        init()
        return {'FINISHED'}

# Bouton permettant de créer les textures
class boutonText(bpy.types.Operator):
    bl_label = "boutonText"
    bl_idname = "object.boutontext"
    bl_description = "Permet de créer toutes les textures à partir des dossiers spécifiés au dessus"

    def execute(self, context):
        global matList
        matList = createTextures(filepathFacade,filepathToit)
        return {'FINISHED'}

class boutonRoute(bpy.types.Operator):
    bl_label = "boutonRoute"
    bl_idname = "object.boutonroute"
    bl_description = "Donne du volume aux routes et importe les textures"
    
    def execute(self, context):
        createRoads()
        return {'FINISHED'}

# Création du panel front-end
class panel(bpy.types.Panel):
    pass
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Import des textures"
    bl_idname = "panel"
    bl_category = "Textures"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        mytool = scene.my_tool
        row = layout.row()
        row.label(text="Textures")
        row = layout.row()
        row.operator("object.boutonuv", text="Initialisation")
        row = layout.row()
        row.operator("object.bouton", text="Ajouter textures")
        row = layout.row()
        row.operator("object.boutonroute", text="Routes")
        layout.prop(mytool, "group", text="Nb batiments par groupe")
        layout.prop(mytool, "facade", text="facades")
        layout.prop(mytool, "toit", text="toits")
        row = layout.row()
        row.operator("object.boutontext", text="Importer textures")

        # Récupère les chemins d'accès des textures
        global filepathFacade
        filepathFacade = bpy.path.abspath(mytool.facade)
        global filepathToit
        filepathToit = bpy.path.abspath(mytool.toit)

        global buildingGroup
        buildingGroup = mytool.group
            
# Enregistrement des classes

classes = (
    MySettings,
    bouton,
    boutonUV,
    boutonText,
    boutonRoute,
    panel,
)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)

    bpy.types.Scene.my_tool = PointerProperty(type=MySettings)

def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)

    del bpy.types.Scene.my_tool

if __name__ == "__main__":
    register()