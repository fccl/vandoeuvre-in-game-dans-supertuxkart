# Import de l'extension

1. Télécharger le fichier .py de l'extension
2. Menu `Edition` > `Préférences`
3. Dans `Add-ons`
4. Cliquer sur `Installer`
5. Chercher le fichier de l'extension et l'ouvrir
6. S'assurer d'avoir bien téléchargé les textures avec

![image 1](img/md6.png)

# Utilisation de l'extension

L'extension s'utilise grâce au menu `Textures` présent en haut à droite de la fenêtre.

## Pré-requis

1. Il est nécessaire de faire un import Blosm avec les paramètres suivants pour le bon fonctionnement de l'extension (Les coordonnées géographiques peuvent être modifiées)
   ![image 2](img/md7.png)
2. Séléctionner le batiment
3. Passer en mode `édition`
4. Appuyer sur le bouton `p` puis `Par parties isolées`

## Utilisation de l'extension

1. Cliquer sur `Initialisation`, ce bouton effectue toutes les actions préalables pour utiliser les autres fonctionnalités. Il n'est nécessaire qu'une fois après l'import Blosm
2. Créer 2 dossiers qui contiendront respectivement les textures de facades et de toits. Ces textures doivent être au format .png ou .jpg et avoir des dimensions en puissances de 2 (exemples : 512x512 ou 64x1024)
3. Renseigner le chemin d'accès de ces 2 dossier dans l'extension avec les champs `facades` et `toits`
4. Une fois les chemin renseigné avec les textures placées, cliquer sur `Importer Textures`, afin d'avoir les textures utilisables dans le logiciels
5. (optionnel) Le curseur `Nb de batiments par groupe` permet d'appliquer la même texture à ce nombre de batiments avant de passer à la texture suivante choisie au hasard (exemple : s'il y a 50 batiments et 5 batiments par groupe, il y aura 10 textures différentes). Le placer à 1 permet d'avoir des batiments ayant tous des textures aléatoires et différentes
6. Une fois que tout est prêt, cliquer sur `Ajouter Textures` pour ajouter toutes les textures importées aux batiments
7. Il est aussi possible d'utiliser le bouton `Routes` Permettant d'importer les textures sur les routes et de leur donner du volume (attention : utiliser plusieurs fois cette fonctionnalité peut provoquer des problèmes). De plus, des modifications manuelles peuvent être nécessaires

## Utilisation de l'extension (version 2)

Les prérequis nécessaires sont les mêmes que pour la version 1.

1. Il est cependant nécessaire de faire un second import Blosm en 3D réaliste (il faut de nouveau réaliser la manipulation pour séparer les batiments sur cet import)
![image 2](img/md15.png)
2. Cliquer sur `Initialisation`, ce bouton effectue toutes les actions préalables pour utiliser les autres fonctionnalités. Il n'est nécessaire qu'une fois après l'import Blosm
3. Cliquer sur `Routes` puis directement sur `Ajouter textures`, le fait de la faire rapidement permet d'éviter des crashs
4. Sauvegarder le fichier Blender, l'étape suivante peut aussi mener à des crashs
5. Cliquer sur `Exporter images`