# Création d'animations

* Pour créer une animation, la majorité des étapes de création sont similaires à celles pour créer un circuit. Il faut donc cocher la case "Is a SuperTuxKart track", puis remplir les cases pour le nom, nom de fichier, etc.
* Il est cependant nécessaire de cocher la case "Is a cutscene" dans les propriétés de la scène. Ceci permet notamment de pouvoir exporter la cinématique sans avoir besoin de d'avoir de tracés de la route et de points de passages.

![image 1](img/md11.png)

* Pour utiliser une scène déja complète, il peut être nécessaire de la recréer du départ en copiant petit à petit les éléments d'un fichier Blender au nouveau.
* Par lui suite, il faut une caméra reconnue par l'extension STK. Pour ce faire, le plus simple est d'ouvrir un fichier d'une animation officielle dans le stk-media-repo, puis de copier la caméra pour ensuite la coller dans la scène.

## Animer la caméra

* Pour animer la caméra, il faut utiliser l'onglet `Animation`
* Selectionner la caméra, la fenêtre du bas représente le temps, il est possible d'ajouter un marqueur avec la touche `I`
* Une fois placé au bon moment de l'animation, placer la caméra à l'endroit souhaité. La caméra effectuera le déplacement entre les 2 points entre les 2 marqueurs.
* Déplacer la caméra à un temps sans marqueur en créé un nouveau.
* Il est possible de gérer la position, la rotation et également l'échelle de la caméra.

![image 2](img/md13.png)

## Animer des objets

* Les étapes pour l'animation sont semblables à celles pour la caméra.
* Pour que l'objet se déplace dans la cinématique, il est nécessaire choisir le Type `Object` dans `SuperTuxKart Object Properties`
* Nommer l'objet puis sélectionner `Ghost` dans `Interaction`

![image 3](img/md12.png)

## Export et lecture de l'animation

* Pour exporter l'animation, il suffit d'utiliser le bouton `Export STK Track`
* Pour pouvoir lire l'animation, son dossier doit se trouver dans le dossier d'installation du jeu (exemple : `/usr/share/games/supertuxkart/data/tracks/`)
* Il faut donc copier le dossier de l'export de l'animation puis le coller dans le dossier cité précedemment. Il peut être nécessaire d'ouvrir les dossiers avec les droits de superutilisateur.

![image 4](img/md14.png)

* Finalement, pour lancer l'animation, exécuter la commande `supertuxkart --cutscene=<cutscene name> --no-start-screen`