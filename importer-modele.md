# Importer un modèle du media repo de STK

## Pré-requis

1. Télécharger le media repo contenant toutes les ressources du jeu (voir la documentation sur [la documentation](https://supertuxkart.net/fr/Media_Repo))
2. Blender version 2.79 (disponible [ici](https://download.blender.org/release/Blender2.79/))
3. Une version récente de blender (ex : 3.4.1)
4. L'extension d'export SuperTuxKart pour Blender
5. Il est nécessaire de placer le dossier de la course dans le media repo dans le dossier `tracks`

## Manipulation sur Blender 2.79

1. Ouvrir le fichier .blend du media repo que vous souhaitez utiliser
2. Passer en mode `Scripting`
![image 1](img/md8.png)
3. Ouvrir le fichier `uv_textures_to_materials.py` présent dans les fichiers de l'extension SuperTuxKart pour Blender dans l'arborescence `/stk-blender-master/extras/`
4. Exécuter le script avec le bouton `Exécuter script`
![image 2](img/md9.png)
5. Sauvegarder le fichier (soit en écrasant le précédant soit dans un nouveau fichier)

## Manipulation sur Blender récent

1. Ouvrir le fichier .blend dans lequel vous souhaitez importer le modèle
2. Être en mode `Objet`
3. `Fichier` > `Ajouter`
   ![image 3](img/md10.png)
4. Aller chercher le fichier .blend enregistré grâce à Blender 2.79 puis ouvrir dans `Object` le modèle souhaité
