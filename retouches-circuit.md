**Retouches**

# Donner du volume aux routes

1. Selectionner la route
2. Onglet `Modeling`
3. Passer en `Mode édition`
4. Touche `a` pour séléctionner toutes les régions
5. Outil `Extruder région`
6. Glisser le `+` jaune pour donner un peu de volume à la route vers le haut

![image 1](img/md1.jpg)

7. Outil `Biseauter`
8. Touche `a`
9. Glisser doucement afin d'avoir une pente pour la route
10. Onglet `Layout`
11. Mode `Déplacer`
12. Ajuster la hauteur de la route

![image 2](img/md2.jpg)

# Agrandir les fondations des batiments

1. Selectionner le batiment 
2. Onglet `Modeling`
3. Passer en `Edit Mode`
4. Mode `Sélection de sommet`
5. Mode `Déplacer`
6. En maintenant la touche `Shift` enfoncée, sélectionner tous les points de la base du batiment
7. Descendre les points grâce au `Curseur`

![image 3](img/md3.jpg)